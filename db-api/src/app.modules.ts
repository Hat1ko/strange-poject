import {DBLModule, TypeOrmModuleProvider} from "./dbl";
import {AddressesModule} from "./modules/addresses";
import {HotelsController} from "./modules/hotels/controllers/hotels.controller";
import {HotelsModule} from "./modules/hotels/hotels.module";
import {OrderRequestsModule} from "./modules/orderRequests/order-requests.module";
import {UsersService} from "./modules/users/services/users.service";
import {UsersModule} from "./modules/users/users.module";

export const ApplicationModules = [
  DBLModule.imports([TypeOrmModuleProvider]),
  AddressesModule,
  HotelsModule,
  UsersModule,
  OrderRequestsModule,
]
