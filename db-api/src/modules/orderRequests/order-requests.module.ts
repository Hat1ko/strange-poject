import {CustomDynamicModule, CustomModule} from "../../shared/helpers/customModule";
import {DBLModule} from "../../dbl";
import {AddressesModule} from "../addresses";
import {OrderRequestsServiceProvider} from "./providers/order-requests-service.provider";
import {OrderRequestsController} from "./controllers/order-requests.controller";

@CustomModule({
  imports: [DBLModule, AddressesModule],
  controllers: [
    OrderRequestsController
  ],
  public: [

  ],
  private: [
    OrderRequestsServiceProvider
  ],
})
export class OrderRequestsModule extends CustomDynamicModule {

}
