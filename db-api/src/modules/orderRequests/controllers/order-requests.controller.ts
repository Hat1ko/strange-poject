import {BadRequestException, Body, Controller, Delete, Get, Inject, Param, Patch, Post} from "@nestjs/common";
import {HOTELS_SERVICE, ORDER_REQUESTS_SERVICE} from "../../../core/providers/providers.const";
import {IHotelsService} from "../../../core/interfaces/hotel/services/hotels-service.inerface";
import {OrderRequestDto} from "../dtos/res/order-request.dto";
import {OrderRequestCreateDto} from "../dtos/req/order-request-create.dto";
import {OrderRequestUpdateDto} from "../dtos/req/order-request-update.dto";
import {IOrderRequestService} from "../../../core/interfaces/orderRequest/services/order-request-service.interface";
import {isUUID} from "class-validator";

@Controller('order-requests')
export class OrderRequestsController {
  constructor(
    @Inject(ORDER_REQUESTS_SERVICE)
    private readonly orderRequestsService: IOrderRequestService
  ) {
  }

  @Post()
  async create( @Body() dto: OrderRequestCreateDto): Promise<OrderRequestDto>{
    return await this.orderRequestsService.create(dto) as OrderRequestDto
  }

  @Get()
  async getAll(): Promise<OrderRequestDto[]> {
    return await this.orderRequestsService.getAll() as OrderRequestDto[]
  }

  @Get(':id')
  async getOne(@Param('id') orderRequestId: string): Promise<OrderRequestDto>{
    if (!isUUID(orderRequestId)) throw new BadRequestException()
    return await this.orderRequestsService.getOne(orderRequestId) as OrderRequestDto
  }

  @Patch(':id')
  async update(@Param('id') orderRequestId: string, @Body() dto: OrderRequestUpdateDto): Promise<OrderRequestDto> {
    if (!isUUID(orderRequestId)) throw new BadRequestException()
    return await this.orderRequestsService.update(orderRequestId, dto) as OrderRequestDto
  }

  @Delete(':id')
  async delete(@Param('id') orderRequestId: string): Promise<void> {
    if (!isUUID(orderRequestId)) throw new BadRequestException()
    await this.orderRequestsService.delete(orderRequestId)
  }
}
