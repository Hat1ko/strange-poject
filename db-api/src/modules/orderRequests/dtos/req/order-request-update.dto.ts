import {IHotelUpdate} from "../../../../core/interfaces/hotel/dtos/hotel-update.interface";
import {IOrderRequestUpdate} from "../../../../core/interfaces/orderRequest/dtos/order-request-update.interface";
import {IsNotEmpty, IsNumber, IsOptional, IsString, IsUUID} from "class-validator";

export class OrderRequestUpdateDto implements IOrderRequestUpdate {
  @IsOptional()
  @IsString()
  @IsUUID()
  hotelId?: string
  @IsOptional()
  @IsString()
  cityName?: string
  @IsOptional()
  @IsString()
  countryName?: string
  @IsOptional()
  @IsString()
  comment?: string
  @IsOptional()
  @IsString()
  date?: string
  @IsOptional()
  @IsNumber()
  roomsCount?: number
}
