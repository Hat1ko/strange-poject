import {IHotelCreate} from "../../../../core/interfaces/hotel/dtos/hotel-create.interface";
import {IOrderRequestCreate} from "../../../../core/interfaces/orderRequest/dtos/order-request-create.interface";
import {IsNotEmpty, IsNumber, IsOptional, IsString, IsUUID} from "class-validator";

export class OrderRequestCreateDto implements IOrderRequestCreate {
  @IsNotEmpty()
  @IsString()
  @IsUUID()
  hotelId: string;
  @IsNotEmpty()
  @IsString()
  @IsUUID()
  userId: string;
  @IsNotEmpty()
  @IsString()
  cityName: string;
  @IsNotEmpty()
  @IsString()
  countryName: string;
  @IsNotEmpty()
  @IsString()
  comment: string;
  @IsNotEmpty()
  @IsString()
  date: string;
  @IsNotEmpty()
  @IsNumber()
  roomsCount: number;
}
