import {IHotel} from "../../../../core/interfaces/hotel/entities/hotel.interface";
import {CityDto} from "../../../addresses/dtos/city.dto";
import {IOrderRequest} from "../../../../core/interfaces/orderRequest/entities/order-request.interface";
import {ICity} from "../../../../core/interfaces/city/entities";
import {HotelDto} from "../../../hotels/dtos/res/hotel.dto";
import {UserDto} from "../../../users/dtos/res/user.dto";

export class OrderRequestDto implements IOrderRequest {
  id: string
  hotelId: string
  cityId: string
  userId: string
  comment: string
  date: string
  roomsCount: number

  hotel?: HotelDto
  city?: CityDto
  user?: UserDto
}
