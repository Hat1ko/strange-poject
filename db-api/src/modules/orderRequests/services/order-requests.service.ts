import {Inject, Injectable} from "@nestjs/common";
import {DBLService} from "../../../dbl";
import {CITIES_SERVICE} from "../../../core/providers/providers.const";
import {ICitiesService} from "../../../core/interfaces/city/services";
import {DeepPartial} from "typeorm/index";
import {IOrderRequestService} from "../../../core/interfaces/orderRequest/services/order-request-service.interface";
import {IOrderRequest} from "../../../core/interfaces/orderRequest/entities/order-request.interface";
import {IOrderRequestCreate} from "../../../core/interfaces/orderRequest/dtos/order-request-create.interface";
import {IOrderRequestUpdate} from "../../../core/interfaces/orderRequest/dtos/order-request-update.interface";
import {OrderRequest} from "../../../dbl/orderRequest/entities/order-request.entity";

@Injectable()
export class OrderRequestsService implements IOrderRequestService {
  constructor(
    private readonly dbl: DBLService,
    @Inject(CITIES_SERVICE)
    private readonly citiesService: ICitiesService,
  ) {
  }

  async create(dto: IOrderRequestCreate): Promise<IOrderRequest> {
    const city = await this.citiesService.get(dto.cityName, dto.countryName)
    const hotel = await this.dbl.orderRequestRepository.create({...dto, cityId: city.id})
    return await hotel.save()
  }

  async getAll(): Promise<IOrderRequest[]> {
    return await this.dbl.orderRequestRepository.find({relations: [ 'city', 'hotel', 'user']})
  }

  async getOne(hotelId: string): Promise<IOrderRequest> {
    return await this.dbl.orderRequestRepository.findOneOrError(hotelId, {relations: [ 'city', 'hotel', 'user']})
  }

  async update(hotelId: string, dto: IOrderRequestUpdate): Promise<IOrderRequest> {
    const orderRequest = await this.dbl.orderRequestRepository.findOneOrError(hotelId)
    const updateData: DeepPartial<OrderRequest> = {id: hotelId, ...dto}

    if (dto.cityName && dto.countryName) {
      const city = await this.citiesService.get(dto.cityName, dto.countryName)
      updateData.cityId = city.id
    }

    await this.dbl.orderRequestRepository.merge(
      orderRequest, updateData
    )

    return await this.dbl.orderRequestRepository.findOneOrError(hotelId, {relations: [ 'city', 'hotel', 'user']})
  }

  async delete(hotelId: string): Promise<void> {
    await this.dbl.orderRequestRepository.delete({id: hotelId})
  }
}
