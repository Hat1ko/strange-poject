import {OrderRequestsService} from "../services/order-requests.service";
import {HOTELS_SERVICE, ORDER_REQUESTS_SERVICE} from "../../../core/providers/providers.const";

export const OrderRequestsServiceProvider = {
  provide: ORDER_REQUESTS_SERVICE,
  useClass: OrderRequestsService
}
