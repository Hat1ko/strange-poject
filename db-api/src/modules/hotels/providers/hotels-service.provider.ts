import {HotelsService} from "../services/hotels.service";
import {HOTELS_SERVICE} from "../../../core/providers/providers.const";

export const HotelsServiceProvider = {
  provide: HOTELS_SERVICE,
  useClass: HotelsService
}
