import {IHotel} from "../../../../core/interfaces/hotel/entities/hotel.interface";
import {CityDto} from "../../../addresses/dtos/city.dto";

export class HotelDto implements IHotel {
  id: string;
  name: string;
  cityId: string;

  city?: CityDto;
}
