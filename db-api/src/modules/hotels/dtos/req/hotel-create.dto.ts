import {IHotelCreate} from "../../../../core/interfaces/hotel/dtos/hotel-create.interface";
import {IsNotEmpty, IsString} from "class-validator";

export class HotelCreateDto implements IHotelCreate {
  @IsNotEmpty()
  @IsString()
  cityName: string;
  @IsNotEmpty()
  @IsString()
  countryName: string;
  @IsNotEmpty()
  @IsString()
  name: string;
}
