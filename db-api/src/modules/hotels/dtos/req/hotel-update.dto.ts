import {IHotelUpdate} from "../../../../core/interfaces/hotel/dtos/hotel-update.interface";
import {IsNotEmpty, IsOptional, IsString} from "class-validator";

export class HotelUpdateDto implements IHotelUpdate {
  @IsOptional()
  @IsString()
  name?: string

  @IsOptional()
  @IsString()
  cityName?: string
  @IsOptional()
  @IsString()
  countryName?: string
}
