import {BadRequestException, Body, Controller, Delete, Get, Inject, Param, Patch, Post} from "@nestjs/common";
import {HOTELS_SERVICE} from "../../../core/providers/providers.const";
import {IHotelsService} from "../../../core/interfaces/hotel/services/hotels-service.inerface";
import {HotelDto} from "../dtos/res/hotel.dto";
import {HotelCreateDto} from "../dtos/req/hotel-create.dto";
import {HotelUpdateDto} from "../dtos/req/hotel-update.dto";
import {isUUID} from "class-validator";

@Controller('hotels')
export class HotelsController {
  constructor(
    @Inject(HOTELS_SERVICE)
    private readonly hotelsService: IHotelsService
  ) {
  }

  @Post()
  async create(@Body() dto: HotelCreateDto): Promise<HotelDto>{
    return await this.hotelsService.create(dto) as HotelDto
  }

  @Get()
  async getAll(): Promise<HotelDto[]> {
    return await this.hotelsService.getAll() as HotelDto[]
  }

  @Get(':id')
  async getOne(@Param('id') hotelId: string): Promise<HotelDto>{
    if (!isUUID(hotelId)) throw new BadRequestException()
    return await this.hotelsService.getOne(hotelId) as HotelDto
  }

  @Patch(':id')
  async update(@Param('id') hotelId: string, @Body() dto: HotelUpdateDto): Promise<HotelDto> {
    if (!isUUID(hotelId)) throw new BadRequestException()
    return await this.hotelsService.update(hotelId, dto) as HotelDto
  }

  @Delete(':id')
  async delete(@Param('id') hotelId: string): Promise<void> {
    if (!isUUID(hotelId)) throw new BadRequestException()
    await this.hotelsService.delete(hotelId)
  }
}
