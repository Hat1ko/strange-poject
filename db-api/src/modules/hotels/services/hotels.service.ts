import {IHotelsService} from "../../../core/interfaces/hotel/services/hotels-service.inerface";
import {Inject, Injectable} from "@nestjs/common";
import {IHotelCreate} from "../../../core/interfaces/hotel/dtos/hotel-create.interface";
import {IHotel} from "../../../core/interfaces/hotel/entities/hotel.interface";
import {IHotelUpdate} from "../../../core/interfaces/hotel/dtos/hotel-update.interface";
import {DBLService} from "../../../dbl";
import {CITIES_SERVICE} from "../../../core/providers/providers.const";
import {ICitiesService} from "../../../core/interfaces/city/services";
import {DeepPartial} from "typeorm/index";
import {Hotel} from "../../../dbl/hotel/entities/hotel.entity";

@Injectable()
export class HotelsService implements IHotelsService {
  constructor(
    private readonly dbl: DBLService,
    @Inject(CITIES_SERVICE)
    private readonly citiesService: ICitiesService,
  ) {
  }

  async create(dto: IHotelCreate): Promise<IHotel> {
    const city = await this.citiesService.get(dto.cityName, dto.countryName)
    const hotel = await this.dbl.hotelsRepository.create({...dto, cityId: city.id})
    return await hotel.save()
  }

  async getAll(): Promise<IHotel[]> {
    return await this.dbl.hotelsRepository.find({relations: [ 'city']})
  }

  async getOne(hotelId: string): Promise<IHotel> {
    return await this.dbl.hotelsRepository.findOneOrError(hotelId, {relations: ['city']})
  }

  async update(hotelId: string, dto: IHotelUpdate): Promise<IHotel> {
    const hotel = await this.dbl.hotelsRepository.findOneOrError(hotelId)
    const updateData: DeepPartial<Hotel> = {id: hotelId, ...dto}

    if(dto.cityName && dto.countryName){
      const city = await this.citiesService.get(dto.cityName, dto.countryName)
      updateData.cityId = city.id
    }

    await this.dbl.hotelsRepository.merge(
      hotel, updateData
    )

    return await this.dbl.hotelsRepository.findOneOrError(hotelId, {relations: ['city']})
  }

  async delete(hotelId: string): Promise<void> {
    await this.dbl.hotelsRepository.delete({id: hotelId})
  }
}
