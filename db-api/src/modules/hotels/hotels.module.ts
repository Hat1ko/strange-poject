import {CustomDynamicModule, CustomModule} from "../../shared/helpers/customModule";
import {DBLModule} from "../../dbl";
import {AddressesModule} from "../addresses";
import {HotelsServiceProvider} from "./providers/hotels-service.provider";
import {HotelsController} from "./controllers/hotels.controller";

@CustomModule({
  imports: [DBLModule, AddressesModule],
  controllers: [
    HotelsController
  ],
  public: [

  ],
  private: [
    HotelsServiceProvider
  ],
})
export class HotelsModule extends CustomDynamicModule {

}
