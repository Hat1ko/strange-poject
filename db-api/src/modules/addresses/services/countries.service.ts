import { Injectable } from '@nestjs/common'
import { DBLService } from '../../../dbl/dbl.service'
import {ICountriesService} from "../../../core/interfaces/country/services";
import {ICountry} from "../../../core/interfaces/country/entities";
import {KeyParser} from "../../../shared/key-parser";

@Injectable()
export class CountriesService extends KeyParser implements ICountriesService {
  constructor(private readonly dbl: DBLService) {
    super()
  }

  async get(name: string): Promise<ICountry> {
    const key = this.getKey(name)

    const country = await this.dbl.countriesRepository.findOne({ key })

    return country || (await this.dbl.countriesRepository.save({ key: key, name }))
  }
}
