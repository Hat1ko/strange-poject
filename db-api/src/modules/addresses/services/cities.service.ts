import { Inject, Injectable } from '@nestjs/common'
import {KeyParser} from "../../../shared/key-parser";
import {DBLService} from "../../../dbl";
import {ICountriesService} from "../../../core/interfaces/country/services";
import {ICity} from "../../../core/interfaces/city/entities";
import {COUNTRIES_SERVICE} from "../../../core/providers/providers.const";
import {ICitiesService} from "../../../core/interfaces/city/services";
@Injectable()
export class CitiesService extends KeyParser implements ICitiesService {
  constructor(
    private readonly dbl: DBLService,
    @Inject(COUNTRIES_SERVICE) private readonly countriesService: ICountriesService,
  ) {
    super()
  }

  async get(cityName: string, countryName: string): Promise<ICity> {
    const cityKey = this.getKey(cityName)

    const country = await this.countriesService.get(countryName)

    const city = await this.dbl.citiesRepository.findOne(
      { key: cityKey, countryId: country.id },
      { relations: ['country'] },
    )

    const result: any =
      city ||
      (await this.dbl.citiesRepository.save({
        key: cityKey,
        name: cityName,
        countryId: country.id,
      }))

    result.country = country

    return result
  }
}
