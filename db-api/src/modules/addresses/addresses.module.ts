import { CountriesServiceProvider } from './providers/countries-service.provider'
import { CustomModule, CustomDynamicModule } from '../../shared/helpers/customModule'
import { CitiesServiceProvider } from './providers'

@CustomModule({
  public: [CountriesServiceProvider, CitiesServiceProvider],
})
export class AddressesModule extends CustomDynamicModule {}
