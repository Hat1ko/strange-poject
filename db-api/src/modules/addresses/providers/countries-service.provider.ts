import { CountriesService } from "../services";
import {COUNTRIES_SERVICE} from "../../../core/providers/providers.const";

export const CountriesServiceProvider = {
  provide: COUNTRIES_SERVICE,
  useClass: CountriesService,
}
