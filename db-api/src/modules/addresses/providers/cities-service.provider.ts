import { CitiesService } from "../services/cities.service";
import {CITIES_SERVICE} from "../../../core/providers/providers.const";

export const CitiesServiceProvider = {
  provide: CITIES_SERVICE,
  useClass: CitiesService,
}
