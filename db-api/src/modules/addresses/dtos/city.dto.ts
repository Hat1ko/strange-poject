import {ICity} from "../../../core/interfaces/city/entities";
import {CountryDto} from "./country.dto";

export class CityDto implements ICity {
  id: string;
  key: string;
  name: string;
  countryId: string;

  country: CountryDto
}
