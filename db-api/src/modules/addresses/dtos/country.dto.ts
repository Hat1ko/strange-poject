import {ICountry} from "../../../core/interfaces/country/entities";

export class CountryDto implements ICountry {
  id: string;
  key: string;
  name: string;
}
