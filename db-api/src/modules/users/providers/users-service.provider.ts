import {UsersService} from "../services/users.service";
import {HOTELS_SERVICE, ORDER_REQUESTS_SERVICE, USERS_SERVICE} from "../../../core/providers/providers.const";

export const UsersServiceProvider = {
  provide: USERS_SERVICE,
  useClass: UsersService
}
