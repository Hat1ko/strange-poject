import {CustomDynamicModule, CustomModule} from "../../shared/helpers/customModule";
import {DBLModule} from "../../dbl";
import {AddressesModule} from "../addresses";
import {UsersServiceProvider} from "./providers/users-service.provider";
import {UsersController} from "./controllers/users.controller";

@CustomModule({
  imports: [
    DBLModule,
    AddressesModule,
  ],
  controllers: [
    UsersController
  ],
  public: [

  ],
  private: [
    UsersServiceProvider
  ],
})
export class UsersModule extends CustomDynamicModule {
}
