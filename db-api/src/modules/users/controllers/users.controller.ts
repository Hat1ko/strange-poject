import {BadRequestException, Body, Controller, Delete, Get, Inject, Param, Patch, Post} from "@nestjs/common";
import {HOTELS_SERVICE, ORDER_REQUESTS_SERVICE, USERS_SERVICE} from "../../../core/providers/providers.const";
import {IHotelsService} from "../../../core/interfaces/hotel/services/hotels-service.inerface";
import {UserDto} from "../dtos/res/user.dto";
import {UserCreateDto} from "../dtos/req/user-create.dto";
import {UserUpdateDto} from "../dtos/req/user-update.dto";
import {IOrderRequestService} from "../../../core/interfaces/orderRequest/services/order-request-service.interface";
import {isUUID} from "class-validator";
import {IUsersService} from "../../../core/interfaces/users/services/users-service.interface";

@Controller('order-requests')
export class UsersController {
  constructor(
    @Inject(USERS_SERVICE)
    private readonly usersService: IUsersService
  ) {
  }

  @Post()
  async create(@Body() dto: UserCreateDto): Promise<UserDto>{
    return await this.usersService.create(dto) as UserDto
  }

  @Get()
  async getAll(): Promise<UserDto[]> {
    return await this.usersService.getAll() as UserDto[]
  }

  @Get(':id')
  async getOne(@Param('id') orderRequestId: string): Promise<UserDto>{
    if (!isUUID(orderRequestId)) throw new BadRequestException()
    return await this.usersService.getOne(orderRequestId) as UserDto
  }

  @Patch(':id')
  async update(@Param('id') orderRequestId: string, dto: UserUpdateDto): Promise<UserDto> {
    if (!isUUID(orderRequestId)) throw new BadRequestException()
    return await this.usersService.update(orderRequestId, dto) as UserDto
  }

  @Delete(':id')
  async delete(@Param('id') orderRequestId: string): Promise<void> {
    if (!isUUID(orderRequestId)) throw new BadRequestException()
    await this.usersService.delete(orderRequestId)
  }
}
