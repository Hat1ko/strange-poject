import {IHotelUpdate} from "../../../../core/interfaces/hotel/dtos/hotel-update.interface";
import {IOrderRequestUpdate} from "../../../../core/interfaces/orderRequest/dtos/order-request-update.interface";
import {IsNotEmpty, IsNumber, IsOptional, IsString, IsUUID} from "class-validator";
import {IUserUpdate} from "../../../../core/interfaces/users/dtos/user-update.interface";

export class UserUpdateDto implements IUserUpdate {
  @IsOptional()
  @IsString()
  firstName?: string
  @IsOptional()
  @IsString()
  lastName?: string
  @IsOptional()
  @IsString()
  email?: string
  @IsOptional()
  @IsString()
  password?: string
  @IsOptional()
  @IsString()
  cityName?: string
  @IsOptional()
  @IsString()
  countryName?: string
}
