import {IHotelCreate} from "../../../../core/interfaces/hotel/dtos/hotel-create.interface";
import {IOrderRequestCreate} from "../../../../core/interfaces/orderRequest/dtos/order-request-create.interface";
import {IsNotEmpty, IsNumber, IsOptional, IsString, IsUUID} from "class-validator";
import {IUserCreate} from "../../../../core/interfaces/users/dtos/user-create.interface";

export class UserCreateDto implements IUserCreate {
  @IsString()
  @IsNotEmpty()
  firstName: string
  @IsString()
  @IsNotEmpty()
  lastName: string
  @IsString()
  @IsNotEmpty()
  email: string
  @IsString()
  @IsNotEmpty()
  password: string
  @IsString()
  @IsNotEmpty()
  cityName: string
  @IsString()
  @IsNotEmpty()
  countryName: string
}
