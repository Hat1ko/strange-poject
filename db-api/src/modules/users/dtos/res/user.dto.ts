import {IHotel} from "../../../../core/interfaces/hotel/entities/hotel.interface";
import {CityDto} from "../../../addresses/dtos/city.dto";
import {IOrderRequest} from "../../../../core/interfaces/orderRequest/entities/order-request.interface";
import {ICity} from "../../../../core/interfaces/city/entities";
import {HotelDto} from "../../../hotels/dtos/res/hotel.dto";
import {IUser} from "../../../../core/interfaces/users/entities";

export class UserDto implements IUser {
  id: string
  firstName: string
  lastName: string
  cityId: string
  email: string
  password: string

  city?: CityDto
}
