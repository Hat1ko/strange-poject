import {IHotelsService} from "../../../core/interfaces/hotel/services/hotels-service.inerface";
import {Inject, Injectable} from "@nestjs/common";
import {IHotelCreate} from "../../../core/interfaces/hotel/dtos/hotel-create.interface";
import {IHotel} from "../../../core/interfaces/hotel/entities/hotel.interface";
import {IHotelUpdate} from "../../../core/interfaces/hotel/dtos/hotel-update.interface";
import {DBLService} from "../../../dbl";
import {CITIES_SERVICE} from "../../../core/providers/providers.const";
import {ICitiesService} from "../../../core/interfaces/city/services";
import {DeepPartial} from "typeorm/index";
import {Hotel} from "../../../dbl/hotel/entities/hotel.entity";
import {IOrderRequestService} from "../../../core/interfaces/orderRequest/services/order-request-service.interface";
import {IOrderRequest} from "../../../core/interfaces/orderRequest/entities/order-request.interface";
import {IOrderRequestCreate} from "../../../core/interfaces/orderRequest/dtos/order-request-create.interface";
import {IOrderRequestUpdate} from "../../../core/interfaces/orderRequest/dtos/order-request-update.interface";
import {OrderRequest} from "../../../dbl/orderRequest/entities/order-request.entity";
import {IUsersService} from "../../../core/interfaces/users/services/users-service.interface";
import {IUser} from "../../../core/interfaces/users/entities";
import {IUserCreate} from "../../../core/interfaces/users/dtos/user-create.interface";
import {IUserUpdate} from "../../../core/interfaces/users/dtos/user-update.interface";

@Injectable()
export class UsersService implements IUsersService {
  constructor(
    private readonly dbl: DBLService,
    @Inject(CITIES_SERVICE)
    private readonly citiesService: ICitiesService,
  ) {
  }

  async create(dto: IUserCreate): Promise<IUser> {
    const city = await this.citiesService.get(dto.cityName, dto.countryName)
    const hotel = await this.dbl.usersRepository.create({...dto, cityId: city.id})
    return await hotel.save()
  }

  async getAll(): Promise<IUser[]> {
    return await this.dbl.usersRepository.find({relations: ['city']})
  }

  async getOne(userId: string): Promise<IUser> {
    return await this.dbl.usersRepository.findOneOrError(userId, {relations: ['city']})
  }

  async update(userId: string, dto: IUserUpdate): Promise<IUser> {
    const orderRequest = await this.dbl.usersRepository.findOneOrError(userId)
    const updateData: DeepPartial<OrderRequest> = {id: userId, ...dto}

    if(dto.cityName && dto.countryName){
      const city = await this.citiesService.get(dto.cityName, dto.countryName)
      updateData.cityId = city.id
    }

    await this.dbl.usersRepository.merge(
      orderRequest, updateData
    )

    return await this.dbl.usersRepository.findOneOrError(userId, {relations: ['city']})
  }

  async delete(userId: string): Promise<void> {
    await this.dbl.orderRequestRepository.delete({id: userId})
  }
}
