export interface ICityFilter {
  id?: string,
  key?: string,
  countryId?: string,
}
