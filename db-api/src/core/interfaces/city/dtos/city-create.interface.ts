export interface ICreateCity {
  key: string;
  name: string;
  countryId: string;
}
