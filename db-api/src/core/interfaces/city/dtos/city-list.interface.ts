import { ICity } from '..';

export interface ICityList {
  items: ICity[],
  count: number,
}
