export interface ICityUpdate {
  id: string
  name?: string;
  key?: string;
  countryId?: string;
}
