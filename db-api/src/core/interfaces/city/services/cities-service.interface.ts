import { ICity } from '../entities'

export interface ICitiesService {
  get(cityName: string, countryName: string): Promise<ICity>
}
