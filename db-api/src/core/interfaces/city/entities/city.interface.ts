import { ICountry } from "../../country";

export interface ICity {
  id: string
  key: string
  name: string
  countryId: string
  country?: ICountry
}
