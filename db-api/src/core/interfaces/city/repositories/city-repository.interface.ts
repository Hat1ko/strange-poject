import { FindConditions, FindOneOptions, ObjectID } from 'typeorm/index'
import { City } from '../../../../dbl/addresses/entities'
import { ExtendedRepository } from '../../../../dbl';

export interface ICitiesRepository extends ExtendedRepository<City> {
  findOneOrError(
    conditions: string | number | ObjectID | FindConditions<City>,
    options?: FindOneOptions<City>,
  ): Promise<City>

  findByNameOrCreate(name: string, countryId: string): Promise<City>

  removeById(id: string): Promise<void>
}
