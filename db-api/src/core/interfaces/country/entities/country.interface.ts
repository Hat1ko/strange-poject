export interface ICountry {
  id: string
  key: string
  name: string
}
