export interface ICountryUpdate {
  id: string;
  key?: string;
  name?: string;
}
