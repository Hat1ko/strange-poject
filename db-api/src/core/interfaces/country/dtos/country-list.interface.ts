import { ICountry } from '..';

export interface ICountryList {
  items: ICountry[];
  count: number;
}
