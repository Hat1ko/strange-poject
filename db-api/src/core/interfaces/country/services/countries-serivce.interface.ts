import { ICountry } from '..'

export interface ICountriesService {
  get(name: string): Promise<ICountry>
}
