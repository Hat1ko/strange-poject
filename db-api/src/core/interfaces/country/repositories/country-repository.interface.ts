import { FindConditions, FindOneOptions, ObjectID } from 'typeorm/index'
import { Country } from '../../../../dbl/addresses/entities/country.entity'
import { ExtendedRepository } from '../../../../dbl';

export interface ICountriesRepository extends ExtendedRepository<Country> {
  findOneOrError(
    conditions: string | number | ObjectID | FindConditions<Country>,
    options?: FindOneOptions<Country>,
  ): Promise<Country>

  findByKeyOrError(key: string): Promise<Country>

  findByNameOrCreate(name: string): Promise<Country>

  removeById(id: string): Promise<void>
}
