import {ICity} from "../../city/entities";

export interface IHotel {
  id: string
  name: string
  cityId: string

  city?: ICity
}
