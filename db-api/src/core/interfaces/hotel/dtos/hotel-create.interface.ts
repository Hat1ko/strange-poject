export interface IHotelCreate {
  name: string
  cityName: string
  countryName: string
}
