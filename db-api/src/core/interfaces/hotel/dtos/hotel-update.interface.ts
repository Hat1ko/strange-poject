export interface IHotelUpdate {
  name?: string
  cityName?: string
  countryName?: string
}
