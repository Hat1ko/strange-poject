import {ExtendedRepository} from "../../../../dbl";
import {Hotel} from "../../../../dbl/hotel/entities/hotel.entity";

export interface IHotelsRepository extends ExtendedRepository<Hotel> {

}
