import {IHotel} from "../entities/hotel.interface";
import {IHotelCreate} from "../dtos/hotel-create.interface";
import {IHotelUpdate} from "../dtos/hotel-update.interface";

export interface IHotelsService {
  create(dto: IHotelCreate): Promise<IHotel>
  getOne(hotelId: string): Promise<IHotel>
  getAll(): Promise<IHotel[]>
  update(hotelId: string, dto: IHotelUpdate): Promise<IHotel>
  delete(hotelId: string): Promise<void>
}
