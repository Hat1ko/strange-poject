import {OrderRequest} from "../../../../dbl/orderRequest/entities/order-request.entity";
import {ExtendedRepository} from "../../../../dbl";

export interface IOrderRequestRepository extends ExtendedRepository<OrderRequest> {

}
