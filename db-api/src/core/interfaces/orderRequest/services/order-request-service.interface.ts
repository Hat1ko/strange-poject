import {IOrderRequest} from "../entities/order-request.interface";
import {IOrderRequestCreate} from "../dtos/order-request-create.interface";
import {IOrderRequestUpdate} from "../dtos/order-request-update.interface";

export interface IOrderRequestService {
  create(dto: IOrderRequestCreate): Promise<IOrderRequest>
  getOne(hotelId: string): Promise<IOrderRequest>
  getAll(): Promise<IOrderRequest[]>
  update(hotelId: string, dto: IOrderRequestUpdate): Promise<IOrderRequest>
  delete(hotelId: string): Promise<void>
}
