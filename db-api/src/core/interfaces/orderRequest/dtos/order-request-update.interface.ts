export class IOrderRequestUpdate {
  hotelId?: string
  cityName?: string
  countryName?: string
  comment?: string
  date?: string
  roomsCount?: number
}
