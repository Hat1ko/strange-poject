export interface IOrderRequestCreate {
  hotelId: string
  userId: string
  cityName: string
  countryName: string
  comment: string
  date: string
  roomsCount: number

}
