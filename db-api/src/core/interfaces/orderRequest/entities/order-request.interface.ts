import {IHotel} from "../../hotel/entities/hotel.interface";
import {ICity} from "../../city/entities";
import {IUser} from "../../users/entities";

export interface IOrderRequest {
  id: string
  hotelId: string
  userId: string
  cityId: string
  comment: string
  date: string
  roomsCount: number

  hotel?: IHotel
  city?: ICity
  user?: IUser
}
