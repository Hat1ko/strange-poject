export interface IUserUpdate {
  firstName?: string
  lastName?: string
  email?: string
  password?: string
  cityName?: string
  countryName?: string
}
