import {ExtendedRepository} from "../../../../dbl";
import {User} from "../../../../dbl/users/entities/user.entity";


export interface IUsersRepository extends ExtendedRepository<User> {

}
