import {IUserCreate} from "../dtos/user-create.interface";
import {IUser} from "../entities";
import {IUserUpdate} from "../dtos/user-update.interface";

export interface IUsersService {
  create(dto: IUserCreate): Promise<IUser>
  getOne(userId: string): Promise<IUser>
  getAll(): Promise<IUser[]>
  update(userId: string, dto: IUserUpdate): Promise<IUser>
  delete(userId: string): Promise<void>
}
