import {ICity} from "../../city/entities";

export interface IUser {
  id: string
  firstName: string
  lastName: string
  cityId: string
  email: string
  password: string

  city?: ICity
}
