export class KeyParser { 
    protected getKey(name: string): string {
        return name.toLowerCase().replace(/ /g, "_");
    }
}