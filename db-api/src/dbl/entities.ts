import {City} from "./addresses/entities";
import {Country} from "./addresses/entities/country.entity";
import {Hotel} from "./hotel/entities/hotel.entity";
import {User} from "./users/entities/user.entity";
import {OrderRequest} from "./orderRequest/entities/order-request.entity";

export const Entities = [
  City,
  Country,
  Hotel,
  User,
  OrderRequest,
]
