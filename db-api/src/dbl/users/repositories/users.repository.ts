import {IUsersRepository} from "../../../core/interfaces/users/repositories/users-repository.interface";
import {User} from "../entities/user.entity";
import {EntityRepository} from "typeorm/index";
import {ExtendedRepository} from "../../extended.repository";

@EntityRepository(User)
export class UsersRepository extends ExtendedRepository<User> implements IUsersRepository {

}
