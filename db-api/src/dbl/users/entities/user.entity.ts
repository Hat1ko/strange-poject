import {BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm/index";
import {IUser} from "../../../core/interfaces/users/entities";
import {City} from "../../addresses/entities";

@Entity('user')
export class User extends BaseEntity implements IUser {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  firstName: string;
  @Column()
  lastName: string;
  @Column()
  cityId: string;
  @Column()
  email: string;
  @Column()
  password: string;

  @ManyToOne(() => City, {onUpdate: 'CASCADE', onDelete: 'RESTRICT'})
  @JoinColumn({name: 'cityId'})
  city?: City
}
