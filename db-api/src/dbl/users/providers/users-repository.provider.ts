import {USERS_REPOSITORY} from "../../../core/providers/providers.const";
import {UsersRepository} from "../repositories/users.repository";
import {Connection} from "typeorm/index";

export const UsersRepositoryProvider = {
  provide: USERS_REPOSITORY,
  useFactory: (connection: Connection) => connection.getCustomRepository(UsersRepository),
  inject: [Connection],
}
