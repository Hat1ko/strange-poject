import {Hotel} from "../entities/hotel.entity";
import {IHotelsRepository} from "../../../core/interfaces/hotel/repositories/hotels-repository.interface";
import {EntityRepository} from "typeorm/index";
import {ExtendedRepository} from "../../extended.repository";

@EntityRepository(Hotel)
export class HotelsRepository extends ExtendedRepository<Hotel> implements IHotelsRepository{

}

