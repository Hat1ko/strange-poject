import {HotelsRepository} from "../repositories/hotels.repository";
import {Connection} from "typeorm/index";
import {HOTELS_REPOSITORY} from "../../../core/providers/providers.const";

export const HotelsRepositoryProvider = {
    provide: HOTELS_REPOSITORY,
    useFactory: (connection: Connection) => connection.getCustomRepository(HotelsRepository),
    inject: [Connection],
}
