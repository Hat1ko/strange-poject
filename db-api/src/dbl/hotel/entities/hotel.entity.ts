import {IHotel} from "../../../core/interfaces/hotel/entities/hotel.interface";
import {BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm/index";
import {City} from "../../addresses/entities";

@Entity('hotel')
export class Hotel extends BaseEntity implements IHotel {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  name: string;
  @Column()
  cityId: string;

  @ManyToOne(() => City, {onUpdate: 'CASCADE', onDelete: 'RESTRICT'})
  @JoinColumn({name: 'cityId'})
  city?: City;

}
