import {ORDER_REQUEST_REPOSITORY} from "../../../core/providers/providers.const";
import {Connection} from "typeorm/index";
import {OrderRequestRepository} from "../repositories/order-request.repository";

export const OrderRequestRepositoryProvider = {
  provide: ORDER_REQUEST_REPOSITORY,
  useFactory: (connection: Connection) => connection.getCustomRepository(OrderRequestRepository),
  inject: [Connection],
}
