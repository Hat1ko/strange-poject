import {EntityRepository} from "typeorm/index";
import {OrderRequest} from "../entities/order-request.entity";
import {ExtendedRepository} from "../../extended.repository";
import {IOrderRequestRepository} from "../../../core/interfaces/orderRequest/repositories/order-request-repository.interface";

@EntityRepository(OrderRequest)
export class OrderRequestRepository extends ExtendedRepository<OrderRequest> implements IOrderRequestRepository {
  
}
