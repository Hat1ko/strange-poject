import {BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm/index";
import {IOrderRequest} from "../../../core/interfaces/orderRequest/entities/order-request.interface";
import {City} from "../../addresses/entities";
import {Hotel} from "../../hotel/entities/hotel.entity";
import {User} from "../../users/entities/user.entity";

@Entity('order-request')
export class OrderRequest extends BaseEntity implements IOrderRequest {
  @PrimaryGeneratedColumn()
  id: string;
  @Column()
  cityId: string;
  @Column()
  hotelId: string;
  @Column()
  userId: string
  @Column()
  comment: string;
  @Column()
  date: string;
  @Column()
  roomsCount: number;

  @ManyToOne(() => City, {onUpdate: 'CASCADE', onDelete: 'RESTRICT'})
  @JoinColumn({name: 'cityId'})
  city?: City;

  @ManyToOne(() => Hotel, {onUpdate: 'CASCADE', onDelete: 'RESTRICT'})
  @JoinColumn({name: 'hotelId'})
  hotel?: Hotel;

  @ManyToOne(() => User, {onUpdate: 'CASCADE', onDelete: 'RESTRICT'})
  @JoinColumn({name: 'userId'})
  user?: User;
}
