import {Global} from "@nestjs/common";
import {CustomDynamicModule, CustomModule} from "../shared/helpers/customModule";
import {DBLService} from "./dbl.service";
import {ConfigModule} from "../shared/config";
import {CitiesRepositoryProvider} from "./addresses/providers";
import {CountriesRepositoryProvider} from "./addresses/providers/country-repository.provider";
import {HotelsRepositoryProvider} from "./hotel/providers/hotels-repository.provider";
import {UsersRepositoryProvider} from "./users/providers/users-repository.provider";
import {OrderRequestRepositoryProvider} from "./orderRequest/providers/order-request-repository.provider";

@Global()
@CustomModule({
  imports: [ConfigModule],
  public: [
    DBLService,
    CitiesRepositoryProvider,
    CountriesRepositoryProvider,
    HotelsRepositoryProvider,
    UsersRepositoryProvider,
    OrderRequestRepositoryProvider,
  ],
})
export class DBLModule extends CustomDynamicModule {}
