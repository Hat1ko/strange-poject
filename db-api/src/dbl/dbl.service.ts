import {Inject, Injectable} from "@nestjs/common";
import {ICountriesRepository} from "../core/interfaces/country/repositories";
import {ICitiesRepository} from "../core/interfaces/city/repositories";
import {
  CITIES_REPOSITORY,
  COUNTRIES_REPOSITORY,
  HOTELS_REPOSITORY, ORDER_REQUEST_REPOSITORY,
  USERS_REPOSITORY
} from "../core/providers/providers.const";
import {IHotelsRepository} from "../core/interfaces/hotel/repositories/hotels-repository.interface";
import {IUsersRepository} from "../core/interfaces/users/repositories/users-repository.interface";
import {IOrderRequestRepository} from "../core/interfaces/orderRequest/repositories/order-request-repository.interface";

@Injectable()
export class DBLService {
    constructor(
      @Inject(COUNTRIES_REPOSITORY)
      public readonly countriesRepository: ICountriesRepository,
      @Inject(CITIES_REPOSITORY)
      public readonly citiesRepository: ICitiesRepository,
      @Inject(HOTELS_REPOSITORY)
      public readonly hotelsRepository: IHotelsRepository,
      @Inject(USERS_REPOSITORY)
      public readonly usersRepository: IUsersRepository,
      @Inject(ORDER_REQUEST_REPOSITORY)
      public readonly orderRequestRepository: IOrderRequestRepository,
    ) {}
}
