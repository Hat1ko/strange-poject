import { EntityRepository, FindConditions, FindOneOptions, ObjectID } from 'typeorm'
import { City } from '../entities'
import { notFoundException } from '../../../core/constants'
import { ICitiesRepository } from '../../../core/interfaces/city'
import { ExtendedRepository } from '../../extended.repository';

@EntityRepository(City)
export class CitiesRepository extends ExtendedRepository<City> implements ICitiesRepository {
  async findOneOrError(
    conditions: string | number | ObjectID | FindConditions<City>,
    options?: FindOneOptions<City>,
  ): Promise<City> {
    try {
      if (!options) options = {}
      if (!options.where) options.where = {}
      if (typeof options.where === 'object') options.where = { ...options.where }
      return await super.findOneOrFail(conditions as any, options)
    } catch (e) {
      if (e.name === 'EntityNotFound') throw notFoundException(this.metadata.targetName)
      else throw e
    }
  }

  async removeById(id: string): Promise<void> {
    await super.delete({ id })
    return
  }

  async findByNameOrCreate(name: string, countryId: string): Promise<City> {
    try{
      return await this.findOneOrError({name, countryId})
    } catch(e) {
      const key = name.toLowerCase().slice(0, 2);
      return await super.create({key, name, countryId}).save()
    }
  }
}
