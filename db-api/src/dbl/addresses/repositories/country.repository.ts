import { EntityRepository, FindConditions, FindOneOptions, ObjectID } from 'typeorm/index'
import { notFoundException } from '../../../core/constants'
import { ICountriesRepository } from '../../../core/interfaces/country'
import { Country } from '../entities/country.entity'
import { ExtendedRepository } from '../../extended.repository';

@EntityRepository(Country)
export class CountriesRepository extends ExtendedRepository<Country>
  implements ICountriesRepository {
  async findOneOrError(
    conditions: string | number | ObjectID | FindConditions<Country>,
    options?: FindOneOptions<Country>,
  ): Promise<Country> {
    try {
      if (!options) options = {}
      if (!options.where) options.where = {}
      if (typeof options.where === 'object') options.where = { ...options.where }
      return await super.findOneOrFail(conditions as any, options)
    } catch (e) {
      if (e.name === 'EntityNotFound') throw notFoundException(this.metadata.targetName)
      else throw e
    }
  }

  async findByKeyOrError(key: string): Promise<Country> {
    try {
      return await super.findOneOrFail({ where: { key: key } })
    } catch (e) {
      if (e.name === 'EntityNotFound') throw notFoundException(this.metadata.targetName)
      else throw e
    }
  }

  async findByNameOrCreate(name: string): Promise<Country> {
    try{
      return await super.findOneOrError({name})
    } catch (e) {
      const key = name.toLowerCase().slice(0, 2)
      return await super.create({key, name}).save()
    }
  }

  async removeById(id: string): Promise<void> {
    await super.delete({ id })
    return
  }
}
