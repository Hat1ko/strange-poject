import { Connection } from 'typeorm/index'
import { CountriesRepository } from '../repositories/country.repository'
import {COUNTRIES_REPOSITORY} from "../../../core/providers/providers.const";

export const CountriesRepositoryProvider = {
  provide: COUNTRIES_REPOSITORY,
  useFactory: (connection: Connection) => connection.getCustomRepository(CountriesRepository),
  inject: [Connection],
}
