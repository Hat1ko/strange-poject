import { Connection } from 'typeorm'
import { CitiesRepository } from '../repositories'
import {CITIES_REPOSITORY} from "../../../core/providers/providers.const";

export const CitiesRepositoryProvider = {
  provide: CITIES_REPOSITORY,
  useFactory: (connection: Connection) => connection.getCustomRepository(CitiesRepository),
  inject: [Connection],
}
