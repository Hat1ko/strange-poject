import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm/index'
import { ICountry } from '../../../core/interfaces/country'

@Entity('country')
export class Country extends BaseEntity implements ICountry {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column()
  key: string

  @Column()
  name: string
}
