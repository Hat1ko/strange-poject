import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { ICity } from '../../../core/interfaces/city'
import { Country } from './country.entity'

@Entity('city')
export class City extends BaseEntity implements ICity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column()
  key: string

  @Column()
  name: string

  @Column()
  countryId: string

  @ManyToOne(type => Country)
  country?: Country
}
