package mvp.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderRequestDTO {
    private String gotelId;
    private String userId;
    private String cityName;
    private String countryName;
    private String comment;
    private String date;
    private Integer roomsCount;
}
