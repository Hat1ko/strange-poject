package mvp.api.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mvp.api.dto.CityDTO;
import mvp.api.dto.CountryDTO;
import mvp.api.dto.HotelDTO;
import mvp.api.service.HotelService;

@Slf4j
@RestController
@RequestMapping("/hotels")
@RequiredArgsConstructor
public class HotelsController {

    private final HotelService hotelService;

    @PostMapping("/create")
    public void createHotel(@RequestBody HotelDTO hotel) throws IOException {
        log.info("REQUEST TO CREATING hotels {}", hotel.toString());
        hotelService.sendToHotels(hotel);
    }

    @PostMapping("/create/city")
    public void createHotel(@RequestBody CityDTO city) throws IOException {
        log.info("REQUEST TO CREATING city {}", city.toString());
        hotelService.sendToCity(city);
    }

    @PostMapping("/create/country")
    public void createCountry(@RequestBody CountryDTO country) throws IOException {
        log.info("REQUEST TO CREATING city {}", country.toString());
        hotelService.sendToCountry(country);
    }
}
