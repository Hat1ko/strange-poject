package mvp.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.slf4j.Slf4j;
import mvp.api.dto.OrderRequestDTO;

@Slf4j
@RequestMapping("/order/request")
public class OrderRequestController {
    
    @PostMapping
    public void create(OrderRequestDTO orderRequest) {
        log.info("REQUEST TO order {}", orderRequest);
    }

    @GetMapping("/{id}")
    public void get(@PathVariable Integer id) {
        log.info("ID: ", id);
    }
}
