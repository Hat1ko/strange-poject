const navBar = '<nav class="navbar navbar-expand-lg navbar-dark bg-primary">' +
    '    <a class="navbar-brand" href="./index.html">' +
    '        <h1 style="font-size: 30px">Booking</h1>' +
    '    </a>' +
    '    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"' +
    '            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">' +
    '        <span class="navbar-toggler-icon"></span>' +
    '    </button>' +
    '    <div class="collapse navbar-collapse" id="navbarSupportedContent">' +
    '        <ul class="navbar-nav mr-auto">' +
    '            <li id="home-page" class="nav-item">' +
    '                <a class="nav-link" href="./index.html">Home</a>' +
    '            </li>' +
    '            <li id="login-page" class="nav-item">' +
    '                <a class="nav-link" href="./Login.html">Login</a>' +
    '            </li>' +
    '            <li id="registration-page" class="nav-item">' +
    '                <a class="nav-link" href="./Registration.html">Registration</a>' +
    '            </li>' +
    '        </ul>' +
    '    </div>' +
    '</nav>'

$(document).ready(() => {
    $("#navigation-bar").append(navBar)
})