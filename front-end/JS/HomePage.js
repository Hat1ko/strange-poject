$(document).ready(()=>{
    $('#home-page').addClass('active')
    let start = moment().subtract(29, 'days');
    let end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
    }

    $('#reportrange').daterangepicker({}, cb);

    cb(start, end);
})