export const addInfo = (res) => {
    $("main").append(`<div class="list-block">
    <div class="info-block">
        <h3 class="list-title">${res.name}</h3>
        <p class="description">${res.description.length>100?`${res.description.substring(0,100)}...`:res.description}</p>
    </div>
    <div class="image-block">
        <a href="${res.imgUrl}">
            <img class="img" src=${res.imgUrl}" alt="img" />
        </a>
    </div>
    <button class="btn btn-primary">Переглянути</button>
</div>`)
}