function updatePage(res) {
    $("body").append(`<header class="title-page">
    <h1 style="text-align: center">${res.title}</h1>
    <p style="text-align: center">${res.country} ${res.city}</p>
</header>
<main class="content">
    <div class="d-flex justify-content-around">
        <div class="w-50">
           ${res.description}
        </div>
        <div>
            <img src="${res.imgUrl}" alt="hotel" class="image" />
        </div>
    </div>
    <div style="margin: auto; width: fit-content">
        <a href="${res.hotelSiteUrl}" class="btn btn-primary button">Перейти</a>
    </div>
</main>`)
}